<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

if (!empty($_SESSION['login']))
{
    $_SESSION = array();

    setcookie('save', '', -100000);
    setcookie('login', '', -100000);
    setcookie('pass', '', -100000);
    session_destroy();
    header('Location: index.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>

    <head>
        <title>Autorization</title>
    </head>
    <div class="Form">
        <form action="" method="post">
            Логин:<input name="login"/>
            Пароль:<input name="pass" type="password"/>
            <input type="submit" value="Войти" />
        </form>
    </div>


    <?php
}
else {
    $user = 'u24007';
    $pass = '46456345';
    $db = new PDO('mysql:host=localhost;dbname=u24007', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $login = htmlspecialchars($_POST['login'], ENT_QUOTES, 'UTF-8');
    $pass = htmlspecialchars($_POST['pass'], ENT_QUOTES, 'UTF-8');

    $stmt = $db->prepare("SELECT * FROM users WHERE login LIKE ?");
    $stmt->execute([$login]);
    $flag=0;
    while($row = $stmt->fetch())
    {
        if(!strcasecmp($row['login'], $login)&&password_verify($pass,$row['hash']))
        {
            $flag=1;
            $user_id=$row['user_id'];
        }
    }
    if($flag) {
        $_SESSION['login'] = $login;
        $_SESSION['uid'] = $user_id;
        header('Location: index.php');
    }
    else{
        header('Location: login.php');
    }
}
