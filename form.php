<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>7thHomework</title>
    <style>
        .error {
            border: 2px solid red;
        }
        .error-radio
        {
            border: 2px solid red;
        }
        .error-bio
        {
            border: 2px solid #ff0000;
            width: 250px;
            height: 16px;
        }
        .error-check
        {
            border: 2px solid red;
            width: 350px;
        }
        .error-abil
        {
            border: 2px solid red;
            width: 200px;
            height: 70px;
        }
        .error-radio-limb
        {
            border: 2px solid red;
            width: 250px;
            height:35px;
        }
    </style>
</head>

<body>
    <div class="modal" data-modal="1">
        <?php
        if (!empty($messages)) {
            print('<div id="messages">');
            foreach ($messages as $message) {
                print($message);
            }
            print('</div>');
        }
        ?>
    
        <button onclick="document.location='login.php'">
            <?php
                if (empty($_SESSION['login'])) {
                    echo "Авторизация";
                } else {
                    echo "Выход";
                }
            ?>
        </button><br/>
        <button onclick="document.location='admin.php'">Админ</button>

        <form action="index.php" id="my-formcarry" accept-charset="UTF-8" class="main" method="POST">
        <input name="csrf_token" type="hidden" value="'.generate_form_token().'"/>
        <label <?php if ($errors['fio']) {print 'class="error-radio"';} ?>>
        <input style="margin-bottom : 1em" id="formname" type="text" name="fio" placeholder="Введите имя" value="<?php print $values['fio']; ?>">
        </label>
        <label <?php if ($errors['fio']) {print 'class="error-radio"';} ?>>
        <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
               placeholder="Введите почту"
            value="<?php print $values['email']; ?>">
        </label>
        <label><br/>
            <input <?php if ($errors['year']) {print 'class="error-radio"';} ?> value="<?php print $values['year_value']; ?>" id="dr" name="birthyear" value="" type="number" placeholder="Год рождения"/>
        </label><br/><br/>
    
    
            Выберите пол:<br/>
            <label <?php if($errors['sex']){print 'class="error-radio"';}?>>
                <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio" name="radio2" value="man"/>
                Мужской</label>
            <label <?php if($errors['sex']){print 'class="error-radio"';}?>> <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio" name=  "radio2" value="woman"/>
                Женский</label>
            <br/>
            <div <?php if($errors['limb']){print 'class="error-radio-limb"';}?>>
                Сколько у вас конечностей :<br/>
                <label><input
                        <?php if($values['limb_value']=="3"){print 'checked';}?>
                            type="radio"
                            name="radio1" value="3"/>
                    3</label>
                <label><input
                        <?php if($values['limb_value']=="4"){print 'checked';}?>
                            type="radio"
                            name="radio1" value="4"/>
                    4</label>
                <br>
            </div>
            <label>
                Ваши сверхспособности:
                <br/>
                <div <?php if ($errors['abil']) {print 'class="error-abil"';} ?>> 
                    <select id="sp" name="superpower[]" multiple="multiple">
                        <option <?php if(in_array("1", $values['abil_value']))print 'selected';?> value="1">Полет</option>
                        <option <?php if(in_array("2", $values['abil_value']))print 'selected';?> value="2" >Бессмертие</option>
                        <option <?php if(in_array("3", $values['abil_value']))print 'selected';?> value="3">Чтение мыслей</option>
                        <option <?php if(in_array("4", $values['abil_value']))print 'selected';?> value="4">Телекинез</option>
                        <option <?php if(in_array("5", $values['abil_value']))print 'selected';?> value="5">Телепортация</option>
                    </select> </div>
            </label><br/>
    
            <label <?php if ($errors['bio']) {print 'class="error-bio"';} ?>>
                Напишите про себя: <br/>
                <textarea id="biog" name="textarea1" placeholder="Пиши тут"><?php print $values['bio_value'];?></textarea>
            </label><br/>
    
            <div <?php if ($errors['check']) {print 'class="error-check"';} ?>><label><input <?php if($values['check_value']=="1"){print 'checked';}?> style="  margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox" value="1">Согласие на обработку персональных данных</label> </div>
    
            <input type="submit" style="margin-bottom : -1em" id="formsend" class="buttonform" value="Отправить">
        </form>
    </div>
</body>
